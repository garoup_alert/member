package kr.hbh.member.domain.store;

import kr.hbh.member.domain.entity.User;

import java.util.List;

public interface UserStore {

    void save(User user);

    boolean existsById(String userId);

    List<User> getUserByUserId(List<String> userIds);

    List<User> getAll();

    long count();
}