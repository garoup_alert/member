package kr.hbh.member.domain.entity;

import lombok.Data;


@Data
public class User {

	private String userId;

	private long chatId;

}
