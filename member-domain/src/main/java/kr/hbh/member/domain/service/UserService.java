package kr.hbh.member.domain.service;

import kr.hbh.member.domain.entity.User;

import java.util.List;

public interface UserService {

    int addUser(User user);

    boolean existsById(String userId);

    List<User> getUserByUserId(List<String> userIds);

    List<User> getAll();

    long count();

}