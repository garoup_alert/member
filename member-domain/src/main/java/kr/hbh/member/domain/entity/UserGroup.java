package kr.hbh.member.domain.entity;

import lombok.Data;


@Data
public class UserGroup {

	private String groupId;

	private String userId;

}
