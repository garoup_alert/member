package kr.hbh.member.domain.store;

import kr.hbh.member.domain.entity.Group;

import java.util.List;

public interface GroupStore {

    void save(Group group);

    boolean existsById(String groupId);

    List<Group> getAll();

    long count();
}