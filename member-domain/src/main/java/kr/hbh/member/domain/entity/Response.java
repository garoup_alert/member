package kr.hbh.member.domain.entity;

import lombok.Data;


@Data
public class Response {

	enum result { SUCCESS, FAIL }

	public void Success(Object data){
		status = result.SUCCESS;
		this.data = data;
	}

	public void Fail(String errMsg){
		status = result.FAIL;
		this.errMsg = errMsg;
	}

	private result status;

	private String errMsg;

	private Object data;

}
