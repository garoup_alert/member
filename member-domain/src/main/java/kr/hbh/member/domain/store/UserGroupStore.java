package kr.hbh.member.domain.store;

import kr.hbh.member.domain.entity.UserGroup;

import java.util.List;

public interface UserGroupStore {

    void save(UserGroup userGroup);

    int remove(UserGroup userGroup);

    int existsById(UserGroup userGroup);

    List<UserGroup> getUserGroupByGroupId(List<String> groupIds);

}