package kr.hbh.member.domain.service;

import kr.hbh.member.domain.entity.UserGroup;

import java.util.List;

public interface UserGroupService {

    int addUserGroup(UserGroup userGroup);

    int removeUserGroup(UserGroup userGroup);

    List<UserGroup> getUserGroupByGroupId(List<String> groupIds);

}