package kr.hbh.member.domain.service;

import kr.hbh.member.domain.entity.Group;

import java.util.List;

public interface GroupService {

    int addGroup(Group group);

    boolean existsById(String groupId);

    List<Group> getAll();

    long count();

}