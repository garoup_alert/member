package kr.hbh.member.rest;

import kr.hbh.member.domain.entity.User;
import kr.hbh.member.domain.entity.Response;
import kr.hbh.member.domain.service.UserService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1.0/member/user")
@Slf4j
public class UserResource {

    private final UserService userService;

    public UserResource(UserService userService){
        this.userService = userService;
    }

    @PostMapping
    public Response addUser(@RequestBody User user) {
        log.info("IF-USER-001 Start!");
        Response res = new Response();

        try{
            //Validation
            if(user.getUserId() == null || "".equals(user.getUserId())){
                res.Fail("[UserId] cannot not be null!");
                return res;
            }

            //add User
            int result = userService.addUser(user);

            //result
            if(result == 0){
                res.Fail("already exist [" + user.getUserId() + "]");
            } else {
                res.Success(result);
            }
        } catch (Exception e){
            log.error("IF-USER-001 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }

    @GetMapping("/{userId}")
    public Response getUserById(@PathVariable("userId") String userId) {
        log.info("IF-USER-002 Start!");
        Response res = new Response();
        try{
            String[] arr = userId.split(",");

            //get User By UserIds
            List<User> list = userService.getUserByUserId(Arrays.asList(arr));

            //result
            res.Success(list);
        } catch (Exception e){
            log.error("IF-USER-002 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }

    @GetMapping
    public Response getAllUser() {
        log.info("IF-USER-003 Start!");
        Response res = new Response();
        try{
            //get All User
            res.Success(userService.getAll());
        } catch (Exception e){
            log.error("IF-USER-003 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }

    @GetMapping("/test/{count}")
    public Response addTestData(@PathVariable("count") int count) {
        log.info("IF-USER-004 Start!");
        Response res = new Response();
        if(count > 1000){
            res.Fail("한번 생성 시 최대 1,000을 넘을 수 없습니다.");
        } else {
            try{
                long cnt = userService.count();
                int result = 0;
                for(int i = 1 ; i <= count ; i++){
                    User user = new User();
                    user.setUserId("user" + (cnt + i));
                    user.setChatId(10000 + (cnt + i));
                    result += userService.addUser(user);
                }
                res.Success(result);
            } catch (Exception e){
                log.error("IF-USER-004 Error!", e);
                res.Fail(e.getMessage());
            }
        }

        return res;
    }
}