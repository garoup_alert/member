package kr.hbh.member.rest;

import kr.hbh.member.domain.entity.Response;
import kr.hbh.member.domain.entity.UserGroup;
import kr.hbh.member.domain.service.UserGroupService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1.0/member/grouping")
@Slf4j
public class UserGroupResource {

    private final UserGroupService userGroupService;

    public UserGroupResource(UserGroupService userGroupService){
        this.userGroupService = userGroupService;
    }


    @PostMapping
    public Response addUserGroup(@RequestBody UserGroup userGroup) {
        log.info("IF-USER GROUP-001 Start!");
        Response res = new Response();

        try{
            //Validation
            String msg = validationUserGroup(userGroup);
            if(msg != null){
                res.Fail(msg);
            } else {
                //add User Group
                int result = userGroupService.addUserGroup(userGroup);

                //result
                if(result == -2){
                    res.Fail("not exist groupId[" + userGroup.getGroupId() + "]");
                } else if(result == -1){
                    res.Fail("not exist userId[" + userGroup.getUserId() + "]");
                } else if(result == 0){
                    res.Fail("already exist [" + userGroup.getGroupId() + ", " + userGroup.getUserId() + "]");
                } else {
                    res.Success(result);
                }
            }
        } catch (Exception e){
            log.error("IF-USER GROUP-001 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;

    }

    @GetMapping("/{groupId}")
    public Response getUserByGroupId(@PathVariable("groupId") String groupId) {
        log.info("IF-USER GROUP-002 Start!");
        Response res = new Response();
        try{
            String[] arr = groupId.split(",");

            //get User Group By groupIds
            List<UserGroup> list = userGroupService.getUserGroupByGroupId(Arrays.asList(arr));

            //result
            res.Success(list);
        } catch (Exception e){
            log.error("IF-USER GROUP-002 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }

    @DeleteMapping
    public Response removeUserGroup(@RequestBody UserGroup userGroup) {
        log.info("IF-USER GROUP-003 Start!");
        Response res = new Response();

        try{
            //Validation
            String msg = validationUserGroup(userGroup);
            if(msg != null){
                res.Fail(msg);
            } else {
                //remove User
                int result = userGroupService.removeUserGroup(userGroup);

                //result
                if(result == 0){
                    res.Fail("not exist [" + userGroup.getGroupId() + ", " + userGroup.getUserId() + "]");
                } else {
                    res.Success(result);
                }
            }
        } catch (Exception e){
            log.error("IF-USER GROUP-003 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;

    }

    private String validationUserGroup(UserGroup userGroup){
        if(userGroup.getUserId() == null || "".equals(userGroup.getUserId())){
            return "[userId] cannot not be null!";
        }
        if(userGroup.getUserId().length() > 50){
            return "[userId] Max length over!: " + userGroup.getUserId().length();
        }
        if(userGroup.getGroupId() == null || "".equals(userGroup.getGroupId())){
            return "[groupId] cannot not be null!";
        }
        if(userGroup.getGroupId().length() > 50){
            return "[groupId] Max length over!: " + userGroup.getGroupId().length();
        }

        return null;
    }

}