package kr.hbh.member.rest;

import kr.hbh.member.domain.entity.Group;
import kr.hbh.member.domain.entity.Response;
import kr.hbh.member.domain.service.GroupService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("api/v1.0/member/group")
public class GroupResource {

    private final GroupService groupService;

    public GroupResource(GroupService groupService){
        this.groupService = groupService;
    }

    @PostMapping
    public Response addGroup(@RequestBody Group group) {
        log.info("IF-GROUP-001 Start!");
        Response res = new Response();

        try {
            //Validation
            if(group.getGroupId() == null || "".equals(group.getGroupId())){
                res.Fail("[groupId] cannot not be null!");
                return res;
            }
            if(group.getGroupId().length() > 50){
                res.Fail("[groupId] Max length over!: " + group.getGroupId().length());
                return res;
            }

            //add Group
            int result = groupService.addGroup(group);

            //result
            if(result == 0){
                res.Fail("already exist [" + group.getGroupId() + "]");
            } else {
                res.Success(result);
            }

        } catch (Exception e) {
            log.error("IF-GROUP-001 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }

    @GetMapping
    public Response getAllGroup() {
        log.info("IF-GROUP-002 Start!");
        Response res = new Response();
        try{
            //get All Group
            res.Success(groupService.getAll());
        } catch (Exception e){
            log.error("IF-GROUP-002 Error!", e);
            res.Fail(e.getMessage());
        }

        return res;
    }

    @GetMapping("/test/{count}")
    public Response addTestData(@PathVariable("count") int count) {
        log.info("IF-GROUP-003 Start!");
        Response res = new Response();
        if(count > 500){
            res.Fail("한번 생성 시 최대 500을 넘을 수 없습니다.");
        } else {
            try{
                long cnt = groupService.count();
                int result = 0;
                for(int i = 1 ; i <= count ; i++){
                    Group group = new Group();
                    group.setGroupId("group" + (cnt + i));
                    result += groupService.addGroup(group);
                }
                res.Success(result);
            } catch (Exception e){
                log.error("IF-GROUP-003 Error!", e);
                res.Fail(e.getMessage());
            }
        }

        return res;
    }

}