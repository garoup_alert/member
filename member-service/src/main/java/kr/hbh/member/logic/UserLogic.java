package kr.hbh.member.logic;

import kr.hbh.member.domain.entity.User;
import kr.hbh.member.domain.service.UserService;
import kr.hbh.member.domain.store.UserStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserLogic implements UserService {


    private final UserStore userStore;

    public UserLogic(UserStore userStore){
        this.userStore = userStore;
    }

    @Override
    public int addUser(User user) {
        int result = 0;
        if(!existsById(user.getUserId())){
            userStore.save(user);
            result = 1;
        }

        return result;
    }

    @Override
    public boolean existsById(String userId) {
        return userStore.existsById(userId);
    }

    @Override
    public List<User> getUserByUserId(List<String> userIds) {
        return userStore.getUserByUserId(userIds);
    }

    @Override
    public List<User> getAll() {
        return userStore.getAll();
    }

    @Override
    public long count() {
        return userStore.count();
    }

}
