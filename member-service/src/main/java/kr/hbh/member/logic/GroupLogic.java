package kr.hbh.member.logic;

import kr.hbh.member.domain.entity.Group;
import kr.hbh.member.domain.service.GroupService;
import kr.hbh.member.domain.store.GroupStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class GroupLogic implements GroupService {

    private final GroupStore groupStore;

    public GroupLogic(GroupStore groupStore){
        this.groupStore = groupStore;
    }

    @Override
    public int addGroup(Group group) {
        int result = 0;
        if(!existsById(group.getGroupId())){
            groupStore.save(group);
            result = 1;
        }

        return result;
    }

    @Override
    public boolean existsById(String groupId) {
        return groupStore.existsById(groupId);
    }

    @Override
    public List<Group> getAll() {
        return groupStore.getAll();
    }

    @Override
    public long count() {
        return groupStore.count();
    }

}
