package kr.hbh.member.logic;

import kr.hbh.member.domain.entity.UserGroup;
import kr.hbh.member.domain.service.GroupService;
import kr.hbh.member.domain.service.UserGroupService;
import kr.hbh.member.domain.service.UserService;
import kr.hbh.member.domain.store.UserGroupStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserGroupLogic implements UserGroupService {


    private final UserGroupStore userGroupStore;
    private final UserService userService;
    private final GroupService groupService;

    public UserGroupLogic(UserGroupStore userGroupStore, UserService userService, GroupService groupService){
        this.userGroupStore = userGroupStore;
        this.userService = userService;
        this.groupService = groupService;
    }

    @Override
    public int addUserGroup(UserGroup userGroup) {
        //그룹 등록 여부
        if(!groupService.existsById(userGroup.getGroupId())){
            return -2;
        }

        //사용자 등록 여부
        if(!userService.existsById(userGroup.getUserId())){
            return -1;
        }

        //사용자 그룹 등록 여부
        if(userGroupStore.existsById(userGroup) > 0){
            return 0;
        }

        userGroupStore.save(userGroup);

        return 1;
    }

    @Override
    public int removeUserGroup(UserGroup userGroup) {
        return userGroupStore.remove(userGroup);
    }

    @Override
    public List<UserGroup> getUserGroupByGroupId(List<String> groupIds) {
        return userGroupStore.getUserGroupByGroupId(groupIds);
    }

}
