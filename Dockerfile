FROM maven:3-jdk-8-alpine

ENV TZ=Asia/Seoul

WORKDIR /usr/src/app

COPY ./member-boot/target/member-boot-1.0-spring-boot.jar /usr/src/app

EXPOSE 8084

CMD java -jar member-boot-1.0-spring-boot.jar
