package kr.hbh.member;

import kr.hbh.member.domain.entity.UserGroup;
import kr.hbh.member.domain.store.UserGroupStore;
import kr.hbh.member.store.jpo.UserGroupJpo;
import kr.hbh.member.store.repository.UserGroupRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserGroupJpaStore implements UserGroupStore {

    private final UserGroupRepository userGroupRepository;

    public UserGroupJpaStore(UserGroupRepository userGroupRepository){
        this.userGroupRepository = userGroupRepository;
    }

    @Override
    public void save(UserGroup userGroup) {
        UserGroupJpo jpo = new UserGroupJpo(userGroup);
        userGroupRepository.save(jpo);
    }

    @Transactional
    @Override
    public int remove(UserGroup userGroup) {
        return userGroupRepository.deleteByUserIdAndGroupId(userGroup.getUserId(), userGroup.getGroupId());
    }

    @Override
    public int existsById(UserGroup userGroup) {
        return userGroupRepository.countByUserIdAndGroupId(userGroup.getUserId(), userGroup.getGroupId());
    }

    @Override
    public List<UserGroup> getUserGroupByGroupId(List<String> groupIds) {
        return userGroupRepository.findByGroupIdIn(groupIds);
    }

}
