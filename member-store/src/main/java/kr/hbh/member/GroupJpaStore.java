package kr.hbh.member;

import kr.hbh.member.domain.entity.Group;
import kr.hbh.member.domain.store.GroupStore;
import kr.hbh.member.store.jpo.GroupJpo;
import kr.hbh.member.store.repository.GroupRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupJpaStore implements GroupStore {

    private final GroupRepository groupRepository;

    public GroupJpaStore(GroupRepository groupRepository){
        this.groupRepository = groupRepository;
    }

    @Override
    public void save(Group group) {
        GroupJpo jpo = new GroupJpo(group);
        groupRepository.save(jpo);
    }

    @Override
    public boolean existsById(String groupId) {
        return groupRepository.existsById(groupId);
    }

    @Override
    public List<Group> getAll() {
        return groupRepository.findAll().stream().map(GroupJpo::toGroup).collect(Collectors.toList());
    }

    @Override
    public long count() {
        return groupRepository.count();
    }

}
