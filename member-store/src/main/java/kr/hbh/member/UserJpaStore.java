package kr.hbh.member;

import kr.hbh.member.domain.entity.User;
import kr.hbh.member.domain.store.UserStore;
import kr.hbh.member.store.jpo.UserJpo;
import kr.hbh.member.store.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserJpaStore implements UserStore {

    private final UserRepository userRepository;

    public UserJpaStore(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public void save(User user) {
        UserJpo jpo = new UserJpo(user);
        userRepository.save(jpo);
    }

    @Override
    public boolean existsById(String userId) {
        return userRepository.existsById(userId);
    }

    @Override
    public List<User> getUserByUserId(List<String> userIds) {
        return userRepository.findByUserIdIn(userIds).stream().map(UserJpo::toUser).collect(Collectors.toList());
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll().stream().map(UserJpo::toUser).collect(Collectors.toList());
    }

    @Override
    public long count() {
        return userRepository.count();
    }

}