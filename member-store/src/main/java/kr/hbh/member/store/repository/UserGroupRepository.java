package kr.hbh.member.store.repository;

import kr.hbh.member.domain.entity.UserGroup;
import kr.hbh.member.store.jpo.UserGroupJpo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserGroupRepository extends JpaRepository<UserGroupJpo, String> {

    int countByUserIdAndGroupId(String userId, String groupId);

    int deleteByUserIdAndGroupId(String userId, String groupId);

    List<UserGroup> findByGroupIdIn(List<String> groupIds);
}
