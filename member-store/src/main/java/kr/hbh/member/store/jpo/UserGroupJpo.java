package kr.hbh.member.store.jpo;

import kr.hbh.member.domain.entity.UserGroup;
import kr.hbh.member.store.jpo.pk.UserGroupPK;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.IdClass;

@Data
@Entity
@Table(name="TAB_USER_GROUP")
@IdClass(UserGroupPK.class)
public class UserGroupJpo {

	public UserGroupJpo(){}

	public UserGroupJpo(UserGroup userGroup){
		this.userId = userGroup.getUserId();
		this.groupId = userGroup.getGroupId();
	}

	public UserGroup toUserGroup(){
		UserGroup userGroup = new UserGroup();
		userGroup.setUserId(this.userId);
		userGroup.setGroupId(this.groupId);

		return userGroup;
	}

	@Id
	@Column(nullable = false, length = 50)
	private String userId;

	@Id
	@Column(nullable = false, length = 50)
	private String groupId;

}

