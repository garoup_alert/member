package kr.hbh.member.store.jpo.pk;

import java.io.Serializable;

public class UserGroupPK implements Serializable {

    protected String userId;
    protected String groupId;

    public UserGroupPK() {}

    public UserGroupPK(String userId, String groupId) {
        this.userId = userId;
        this.groupId = groupId;
    }

}
