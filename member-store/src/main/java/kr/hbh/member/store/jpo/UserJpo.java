package kr.hbh.member.store.jpo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

import lombok.Data;

import kr.hbh.member.domain.entity.User;

@Data
@Entity
@Table(name="TAB_USER")
public class UserJpo {

	public UserJpo(){ }

	public UserJpo(User user){
		this.userId = user.getUserId();
		this.chatId = user.getChatId();
	}

	public User toUser(){
		User user = new User();
		user.setUserId(this.userId);
		user.setChatId(this.chatId);

		return user;
	}

	@Id
	@Column(nullable = false, length = 50)
	private String userId;

	@Column(nullable = false)
	private long chatId;
}

