package kr.hbh.member.store.repository;

import kr.hbh.member.store.jpo.GroupJpo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<GroupJpo, String> {

}
