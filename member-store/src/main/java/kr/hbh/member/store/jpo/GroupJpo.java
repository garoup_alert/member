package kr.hbh.member.store.jpo;

import kr.hbh.member.domain.entity.Group;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name="TAB_GROUP")
public class GroupJpo {

	public GroupJpo(){}

	public GroupJpo(Group group){
		this.groupId = group.getGroupId();
	}

	public Group toGroup(){
		Group group = new Group();
		group.setGroupId(this.groupId);

		return group;
	}

	@Id
	@Column(nullable = false, length = 50)
	private String groupId;

}

