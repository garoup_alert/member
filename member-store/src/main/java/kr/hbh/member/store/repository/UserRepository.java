package kr.hbh.member.store.repository;

import kr.hbh.member.store.jpo.UserJpo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<UserJpo, String> {

    List<UserJpo> findByUserIdIn(List<String> userIds);

}
